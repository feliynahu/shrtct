using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour
{
    //MENU INICIAL
    public void EmpezarJuego()
    {
        SceneManager.LoadScene("Control Z");
    }

    public void MenuPrincipal()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void ComingSoon()
    {
        Debug.Log("No disponible por el momento");
    }

    public void Final()
    {
        SceneManager.LoadScene("Final");
    }




}


