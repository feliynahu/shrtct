using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Incognito : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject fondo;
    [SerializeField] private bool IncognitoAct;
    void Start()
    {
        IncognitoAct = false;
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.N))
        {
            IncognitoAct = true;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Incognito") && !IncognitoAct)
        {
            fondo.SetActive(true);
            animator.SetTrigger("Piso");
        }
    }
}
