using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Mayuscula : MonoBehaviour
{
    TextMeshPro txtK;
    TextMeshPro txtE;
    TextMeshPro txtY;
    bool mayus;
    [SerializeField] GameObject k;
    [SerializeField] GameObject e;
    [SerializeField] GameObject y;
    void Start()
    {
        mayus = false;
    }

    void Update()
    {
        InputMayus();
    }

    private void InputMayus()
    {
        if (Input.GetKeyDown(KeyCode.CapsLock))
        {
            print("mayuscula");
            if (mayus) mayus = false;
            else if (!mayus) mayus = true;

        }
        if ((Input.GetKey(KeyCode.K) && mayus) || (Input.GetKey(KeyCode.K) && Input.GetKey(KeyCode.LeftShift)))
        {
            k.GetComponent<BoxCollider2D>().size = new Vector2(3f, 3f);
            txtK = k.GetComponent<TextMeshPro>();
            txtK.fontSize = 35;
            txtK.fontStyle = FontStyles.Bold;
        }
        else if ((Input.GetKey(KeyCode.E) && mayus) || (Input.GetKey(KeyCode.E) && Input.GetKey(KeyCode.LeftShift)))
        {
            e.GetComponent<BoxCollider2D>().size = new Vector2(3f, 3f);
            txtE = e.GetComponent<TextMeshPro>();
            txtE.fontSize = 35;
            txtE.fontStyle = FontStyles.Bold;
        }
        else if ((Input.GetKey(KeyCode.Y) && mayus) || (Input.GetKey(KeyCode.Y) && Input.GetKey(KeyCode.LeftShift)))
        {
            y.GetComponent<BoxCollider2D>().size = new Vector2(3f, 3f);
            txtY = y.GetComponent<TextMeshPro>();
            txtY.fontSize = 35;
            txtY.fontStyle = FontStyles.Bold;
        }
    }
    //void OnMouseOver()
    //{
    //    if (Input.GetKeyDown(KeyCode.CapsLock))
    //    {
    //        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(3f, 3f);
    //        txt = gameObject.GetComponent<TextMeshPro>();
    //        txt.fontSize = 35;
    //        txt.fontStyle = FontStyles.Bold;
    //    }

    //}
    //void OnMouseDrag()
    //{
    //    if (Input.GetKeyDown(KeyCode.CapsLock))
    //    {
    //        gameObject.GetComponent<BoxCollider2D>().size = new Vector2(3f,3f);
    //        txt = gameObject.GetComponent<TextMeshPro>();
    //        txt.fontSize = 35;
    //        txt.fontStyle = FontStyles.Bold;
    //    }
    //}
}
