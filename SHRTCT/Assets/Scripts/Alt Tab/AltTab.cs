using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltTab : MonoBehaviour
{
    void Start()
    {
        
    }

    void Update()
    {
        // Verificar si se presion� la tecla Alt y la tecla Tab simult�neamente
        if (Input.GetKeyDown(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.Tab))
        {
            // Anular la acci�n de Alt + Tab
            Debug.Log("Se bloque� la combinaci�n de teclas Alt + Tab.");
            // Aqu� puedes agregar el c�digo o la l�gica que deseas ejecutar cuando se presiona Alt + Tab en tu juego
        }
    }
}
