using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientosJugadores : MonoBehaviour
{
    [SerializeField] GameObject[] Players;
    [SerializeField] GameObject padre;
    BoxCollider2D[] objs;
    GameObject Sube;
    void Start()
    {
        objs = padre.GetComponentsInChildren<BoxCollider2D>();
        Sube = GameObject.Find("Sube");
    }

    void Update()
    {
        if (Camera.main.fieldOfView < 8)
        {
            Players[1].SetActive(true);
            Players[1].GetComponent<ControlPJ>().enabled = true;
            Players[0].GetComponent<ControlPJ>().enabled = false;
            Players[1].GetComponent<BoxCollider2D>().enabled = true;
            Players[0].GetComponent<BoxCollider2D>().enabled = false;
            if(Sube!=null)Sube.GetComponent<BoxCollider2D>().enabled = true;
            Players[0].GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
            Players[0].SetActive(false);
            foreach (BoxCollider2D i in objs)
            {
                i.enabled = true;
            }
            Players[1].GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        }
        else if (Camera.main.fieldOfView > 20)
        {
            Players[0].SetActive(true);
            Players[0].GetComponent<ControlPJ>().enabled = true;
            Players[1].GetComponent<ControlPJ>().enabled = false;
            Players[1].GetComponent<BoxCollider2D>().enabled = false;
            Players[0].GetComponent<BoxCollider2D>().enabled = true;
            if (Sube != null) Sube.GetComponent<BoxCollider2D>().enabled = false;
            Players[1].GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;
            Players[0].GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            Players[1].SetActive(false);
            foreach (BoxCollider2D i in objs)
            {
                i.enabled = false;
            }
        }
    }
}
