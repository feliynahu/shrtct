using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Animaciones : MonoBehaviour
{
    private Animator animator;
    [SerializeField] private AnimationClip animacionUlt;
    [SerializeField] private AnimationClip animacionInicio;
    ControlPJ jugador;
    [SerializeField] int n;
    void Start()
    {
        animator = GetComponent<Animator>();
        jugador = FindObjectOfType<ControlPJ>();
        StartCoroutine(Inicio());
    }
    public void CambioDeEscenasTransicion()
    {

        print("Cambio de escena");
        jugador.speed = 0;
        StartCoroutine(Transicion());
    }
    IEnumerator Transicion()
    {
        print("I");
        animator.SetTrigger("Iniciar");
        yield return new WaitForSeconds(animacionUlt.length);
        SceneManager.LoadScene(n);
    }
    IEnumerator Inicio()
    {
        jugador.speed = 0;
        yield return new WaitForSeconds(animacionInicio.length);
        jugador.speed = 10;
    }
}
