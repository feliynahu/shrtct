using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambioDeEscenas : MonoBehaviour
{

    [SerializeField] GameObject canvas;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        canvas.GetComponent<Animaciones>().CambioDeEscenasTransicion();
    }
}
