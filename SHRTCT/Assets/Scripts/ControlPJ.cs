using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlPJ : MonoBehaviour
{
    private Rigidbody2D rb;

    public float direccion = 0f;
    public float speed = 5f;
    public float fuerzaSalto = 5f;
    public bool estaEnPiso;
    [SerializeField] int n;
    private GameManager gameManager;

    public LayerMask capa;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        estaEnPiso = false;
    }

    private void Update()
    {
        Movement();
        direccion = Input.GetAxisRaw("Horizontal");

        if (Input.GetKeyDown("space") && estaEnPiso)
        {
            rb.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }

        if(Input.GetKeyDown(KeyCode.F5)) SceneManager.LoadScene(n);

        rb.velocity = new Vector2(direccion * speed, rb.velocity.y);

        if (direccion < 0) GetComponent<SpriteRenderer>().flipX = true;
        else GetComponent<SpriteRenderer>().flipX = false;

        if (Input.GetKeyDown(KeyCode.Escape)) SceneManager.LoadScene(0);
    }

    private void FixedUpdate()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "ground")
        {
            estaEnPiso = true;
        }
        else
        {
            estaEnPiso = false;
        }
    }

    private void Movement()
    {

        Debug.DrawLine(transform.position, transform.position + Vector3.down * 1.8f, Color.green);

        if (Physics2D.Raycast(transform.position, Vector2.down, 1.8f, capa))
        {
            estaEnPiso = true;
        }
        else { estaEnPiso = false; }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Sube")
        {
            Destroy(collision.gameObject);
            gameManager = FindObjectOfType<GameManager>();
            gameManager.ActivarObjetivo();
        }
    }



}

