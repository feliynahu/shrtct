using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ControlA : MonoBehaviour
{
    [SerializeField] Letra[] letras;
    [SerializeField] int CantLetras;
    [SerializeField] bool AllAct;
    [SerializeField] GameObject sube;

    public bool AllAct1 { get => AllAct; set => AllAct = value; }

    void Start()
    {
        letras = FindObjectsOfType<Letra>();
        CantLetras = letras.Length;
        AllAct1 = false;
    }

    void Update()
    {
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Letra>() is Letra)
        {
            Destroy(collision.gameObject);
            CantLetras -= 1;
        }
        if (collision.GetComponent<Letra>() is Letra && AllAct1)
        {
            Letra[] G = FindObjectsOfType<Letra>();
            foreach (Letra l in G)
            {
                Destroy(l.gameObject);
                CantLetras = 0;
            }
            sube.SetActive(true);
        }
    }

    public void BorrarLetras()
    {
        foreach (Letra l in letras)
        {
            Destroy(l);
        }
    }
}
