
using UnityEngine;

public class BorrarAltF4 : MonoBehaviour
{
    //[SerializeField] private GameObject confirmationWindow;
    [SerializeField] private GameObject bloqueBorrar;
    //private static ConfirmQuitting instance;


    [RuntimeInitializeOnLoadMethod]
    static void RunOnStart()
    {
        Application.wantsToQuit += CallQuitWindow;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.LeftAlt) && Input.GetKeyDown(KeyCode.F4)) Destroy(bloqueBorrar);
    }
    //private void Awake()
    //{
    //    instance = this;
    //}


    public static bool CallQuitWindow()
    {
        // Call Confirmation-Window and abort the quitting
        //instance.confirmationWindow.SetActive(true);
        print("Hola");
        return false;
    }




    // Attached to Conformation-Button
    public void Confirm()
    {
        // When pressing Confirmation-Button, unsubscribe from Event and the Quit Application
        Application.wantsToQuit -= CallQuitWindow;
        Application.Quit();
    }
}
