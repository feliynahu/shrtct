using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class CopiaPega : MonoBehaviour
{
    public GameObject obj;
    bool act;
    [SerializeField] int contadorDePegas;
    void Start()
    {
        act = false;
    }

    // Update is called once per frame
    void Update()
    {
        contadorDePegas = GameObject.FindObjectsOfType<DragAndDrop>().Length;
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.V) && act && contadorDePegas < 5)
        {
            Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10);
            Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
            Instantiate(obj, objPosition, gameObject.transform.rotation);
            Debug.Log("Pegar");
        }

    }
    void OnMouseDrag()
    {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.C))
        {
            Debug.Log("Copiar");
            act = true;
            obj = gameObject;
        }
    }
}
