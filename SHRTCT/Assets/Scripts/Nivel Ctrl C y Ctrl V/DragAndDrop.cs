using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragAndDrop : MonoBehaviour
{
    float distance = 10;
    Collider2D col;
    Vector3 tp;

    private void Start()
    {
        col = GetComponent<Collider2D>();
        tp = GetComponent<Transform>().position;

    }
    void OnMouseDrag()
    {
        Vector3 mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
        Vector3 objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
        col.isTrigger = true;
        transform.position = objPosition;
        
    }
    void OnMouseUp()
    {
        col.isTrigger = false;
        Debug.Log("Drag ended!");
        
        if(transform.position.x > 26 || transform.position.x < -26)
        {
            transform.position = tp;
            print("TP");
        }
        if (transform.position.y > 15.5f || transform.position.y < -9f)
        {
            transform.position = tp;
            print("TP");
        }
        tp = transform.position;
    }

    private void OnMouseOver()
    {
        GetComponent<Outline>().enabled=true;
    }
    private void OnMouseExit()
    {
        GetComponent<Outline>().enabled = false;
    }

}