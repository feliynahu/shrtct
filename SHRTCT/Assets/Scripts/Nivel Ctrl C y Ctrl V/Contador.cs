using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class Contador : MonoBehaviour
{
    private int contador = 3;
    [SerializeField] TextMeshPro txtcont;
    [SerializeField] GameObject sube;
    private void Start()
    {
        txtcont = GetComponent<TextMeshPro>();
        sube.SetActive(false);
    }
    private void Update()
    {
        if (contador <= 0 && sube)
        {
            sube.SetActive(true);
        }
        else if(contador > 0 && !sube) { sube.SetActive(false); }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Contador"))
        {
            contador--;
            collision.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
            collision.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            txtcont.text = contador.ToString();
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Contador"))
        {
            contador++;
            collision.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            collision.gameObject.GetComponent<Rigidbody2D>().constraints =RigidbodyConstraints2D.FreezePosition;
            txtcont.text = contador.ToString();
        }
    }
    
}
