using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ControlF : MonoBehaviour
{
    [SerializeField] private GameObject bridge;
    [SerializeField] private Animator animator;
    [SerializeField] private TMP_Text txt;
    private void Update()
    {
       if(Input.GetKey(KeyCode.LeftControl)&& Input.GetKey(KeyCode.F))
        {
            animator.SetTrigger("cartel");
        }
    }
    public void ReadInput(string input)
    {
        if (input.ToLower().Equals("bridge"))
        {
            txt.text = 1.ToString()+"/"+ 1.ToString();
            bridge.SetActive(true);
        }
    }
}
