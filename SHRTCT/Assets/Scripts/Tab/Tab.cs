using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class Tab : MonoBehaviour
{
    [SerializeField] GameObject[] cubes;
    int n;
    void Start()
    {
        n = 0;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (n > 4) n = 0;
            print("tab");
            foreach (GameObject g in cubes)
            {
                g.GetComponent<Outline>().enabled = false;
            }
            cubes[n].GetComponent<Outline>().enabled = true;
            n += 1;
        }
       
    }
}
